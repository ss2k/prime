require './lib/prime_num'
# test prime num

def pass(msg)
  print "P - "
  puts msg
end

def fail(msg)
  print "F - "
  puts msg
end

total_specs = 0
failed_specs = 0

## is_prime specs

# spec

msg = "Negative number should return false for prime"
if PrimeNum.is_prime(-1)
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end

total_specs += 1

msg = "0 should return false for prime"
if PrimeNum.is_prime(0)
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end

total_specs += 1


# spec

msg = "1 should return false for prime"
if PrimeNum.is_prime(1)
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end

total_specs += 1


# spec

msg = "2 should return true for prime"
if !PrimeNum.is_prime(2)
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end

total_specs += 1


# spec

msg = "4 should return false for prime"
if PrimeNum.is_prime(4)
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end

total_specs += 1


# spec

msg = "9 should return false for prime"
if PrimeNum.is_prime(9)
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end
total_specs += 1

# spec

msg = "13 should return true for prime"
if !PrimeNum.is_prime(13)
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end
total_specs += 1

## get_first_prime_nums specs

# spec

msg = "first prime number"
if PrimeNum.get_first_prime_nums(1) != [2]
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end
total_specs += 1

# spec

msg = "first 3 prime numbers"
if PrimeNum.get_first_prime_nums(3) != [2,3,5]
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end
total_specs += 1

# spec

msg = "first 5 prime numbers"
if PrimeNum.get_first_prime_nums(5) != [2,3,5,7,11]
  fail(msg)
  failed_specs += 1
else 
  pass(msg)
end
total_specs += 1

puts "Total Specs: #{total_specs}"
puts "Failed Specs: #{failed_specs}"


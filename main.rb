require './lib/print_table'
require './lib/prime_num'

total_amount = ARGV[0].to_i

if total_amount < 1
  puts "Please pass in a positive integer"
else
  nums_to_print = PrimeNum.get_first_prime_nums(total_amount)
  puts "Printing the multiplication table of the first #{total_amount} prime numbers"
  PrintTable.new(nums_to_print).print_table
end
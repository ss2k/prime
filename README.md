# Prime Number Table Generator
This is a simple application that takes in an integer and returns a multiplication table consisting of the first prime numbers upto that integer. For example if you pass in 10, you will get an output of the multiplication of the first 10 prime numbers

## Usage
    $ ruby main.rb NUM
Pass in an argument for the number of prime numbers table you want to see. For example, to see the first 10 prime numbers table you would enter the following:

    $ ruby main.rb 10

## Testing
Instead of using an external testing library, I wrote a simple test using just pure ruby because of the simplicity of this application.  To run the specs just type the following:

    $ ruby test/specs.rb 

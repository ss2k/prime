class PrintTable
  def initialize(nums_to_print)
    @nums_to_print = nums_to_print
  end

  def print_table
    # Print out the header
    print "-\t"
    @nums_to_print.map{ |x| print "#{x}\t" }
    print "\n"
    
    # Print out the multiplication table
    @nums_to_print.map do |x|
      print "#{x}\t"
      @nums_to_print.map do |y|
        print "#{x * y}\t"
      end
      print "\n"
    end
  end
end
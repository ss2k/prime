class PrimeNum
  def self.get_first_prime_nums(num)
    # Return the first x prime numbers
    prime_nums = []
    # Since 2 is the first prime number we start at that
    i = 2
    while prime_nums.size < num do
      prime_nums << i if is_prime(i)
      i+=1
    end
    prime_nums
  end

  def self.is_prime(num)
    # Check if a number is prime
    return false if num < 2
    # Instead of checking each potential divisor, optimize it by taking square root of the number
    2.upto(Math.sqrt(num)) do |n|
      return false if num % n == 0
    end
    true
  end
end